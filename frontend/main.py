from flask import Flask, request, render_template
import requests
import json

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/profile")
def profile():
    id = request.args.get("id")
    url = "http://localhost:9000/" + id
    response = requests.get(url)
    data = response.text
    parsed = json.loads(data)
    return render_template("profile.html", data=parsed)

if __name__ == "__main__":
    app.run(debug=True)