$("#search").click(() => {
    triggerAjax();
});

function triggerAjax() {
    txt_superHero = $("#superHeroName").val();
    $.ajax({
        type: "get",
        url: "http://localhost:9000/search/" + txt_superHero,
        dataType: "json",
        beforeSend: function () {
            $("#loading").css("display", "block");
            $("#listSuperHeros").hide();
        },
        success: function (result) {
            if (result.response === "success") {
                var count = 1;
                var row = "";
                result.results.forEach((data) => {
                    row += "<tr>"
                        + "<td>" + (count++) + "</td>"
                        + "<td>" + data.name + "</td>"
                        + "<td>" + data.biography["full-name"] + "</td>"
                        + "<td><a href='profile?id=" + data.id + "'>view</a></td>"
                        + "</tr>";
                });
                $("#viewSuperHeros").html(row);
            }
            else if(result.response === "error"){
                alert("Name not Found!");
                $("#viewSuperHeros").empty();
            }
        },
        complete: function () {
            $("#loading").hide();
            $("#listSuperHeros").show();
        },
    });
}