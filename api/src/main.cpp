#include "mongoose.h"
#include <string>
#include <iostream>
#include <curl/curl.h>

using namespace std;

mg_mgr mgr;
mg_connection *nc;
static mg_serve_http_opts s_http_server_opts;

static const char *s_http_port = "9000";

size_t write_to_string(void *ptr, size_t size, size_t count, void *stream)
{
    ((string *)stream)->append((char *)ptr, 0, size * count);
    return size * count;
}

void ev_handler(mg_connection *nc, int ev, void *ev_data)
{
    if (ev == MG_EV_HTTP_REQUEST)
    {
        struct http_message *http = (struct http_message *)ev_data;
        std::string uri("nil");
        uri.assign(http->uri.p, http->uri.len);
        cout << "GET " << uri << endl;

        CURL *curl;
        CURLcode res;
        curl = curl_easy_init();
        string response;

        string url = "https://superheroapi.com/api/2810272235696008";
        url = url + uri;
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        mg_printf(nc, "HTTP/1.1 200 OK\r\n"
                      "Content-Type: application/json\r\n"
                      "Access-Control-Allow-Origin: *\r\n"
                      "Access-Control-Allow-Methods: GET\r\n"
                      "Access-Control-Allow-Headers: Content-Type\r\n"
                      "Connection: keep-alive\r\n"
                      "\r\n"
                      "%s",
                  response.c_str());
        nc->flags |= MG_F_SEND_AND_CLOSE;
    }
}

int main()
{
    mg_mgr_init(&mgr, NULL);
    nc = mg_bind(&mgr, s_http_port, ev_handler);
    mg_set_protocol_http_websocket(nc);
    for (;;)
    {
        mg_mgr_poll(&mgr, 1000);
    }
    mg_mgr_free(&mgr);
    return 0;
}