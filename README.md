# The Super Hero App

## Getting Started
These instructions are meant to execute the project in **Mac Machines**.  
  
The project consist of two modules namely **api** and **frontend**, in which all the codes related to *modules goes to their respective directories*.

## API Module - Backend Server
Developed using CPP  
Listening Port: **9000**

### Libraries used
*  [Mongoose](https://github.com/cesanta/mongoose)
*  Curl

### Prerequisites
Install following tools/packages:
*  [Xcode Command Line Tools](https://railsapps.github.io/xcode-command-line-tools.html) - Xcode is a large suite of software development tools and libraries from Apple.
*  [Homebrew](https://brew.sh) - The Missing Package Manager for macOS (or Linux)
*  CMake - `brew install cmake`
*  Ninja - `brew install ninja`
*  Curl - `brew install curl`

### Building Executable
Go to `build` directory in *api* folder. Then,
```
cmake -GNinja ..
ninja
```

### Running Application
Go to `bin` directory in *api* folder. Then,
```
./server
```

## Frontend Server
Developed using Python3  
Listening Port: **5000**

### Libraries used
*  Virtualenv
*  Flask
*  Requests

### Prerequisites
Install following tools/packages:
*  [Python 3.8.1](https://www.python.org/downloads/)
*  Virtualenv - `pip3 install virtualenv`

### Setting up environment
Go to frontend directory. Then,
1.  Create a virtual environment, frontend.
```
python3 -m virtualenv frontend
```
2.  Activate the frontend virtual environment
```
source frontend/bin/activate
```
3.  Install packages in requirements.txt
```
pip3 install -r requirements.txt
```

### Running Application
From inside virtual environment frontend, 
1.  Execute `main.py`
```
python3 main.py
```
2. On web browser, go to URL `http://localhost:5000`